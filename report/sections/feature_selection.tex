% !TeX spellcheck = en_GB
Based on the generated static features the dataset is now represented in a 856x128 matrix. Applying various feature selection algorithms allows to reduce the number of features or combine features into new ones. All feature selection algorithms are only applied on the train data and the result is used to transform the test data.

\subsubsection{Using all features}
Based on the prediction model used feature selection is not necessarily required. Some models, for example \gls{SVM}s, are supposed to perform very well with high-dimensional feature spaces\footnote{\url{https://stats.stackexchange.com/questions/63081/feature-selection-before-svm}} while models like \gls{NB} seem to perform better with reduced dimensionality\footnote{\url{https://iopscience.iop.org/article/10.1088/1742-6596/1511/1/012001}}. This will be further theoretically analysed in the model section \ref{ssec:static_modelselection} while in section \ref{ssec:static_results} the results of applying different feature selection methods will be compared.


\subsubsection{Variance threshold}
Variance is a measure for how much a value in a series of values differs from the mean. Because the mean represents all other numbers, variance can also be seen as how much numbers differ from each other. For applying machine learning models, the goal is to focus on features with the most information by removing features with less information to reduce the data size. In some ways a higher variance can be seen as more information - not necessarily useful information, but it can supply a first reference.\\

Using variance threshold as feature selection applies this idea by calculating the variance of each feature and only using those features with a variance above a certain threshold. For this dataset the threshold is set to $0.9$ which results in 37 features being selected. An implementation using numpy can be found in \textbf{feature\_selection\_own.py} function \textbf{variance\_threshold\_return\_columns\_above\_threshold} while for the main model calculation the VarianceThreshold class supplied by sklearn is used.


\subsubsection{Linear discriminant analysis}
\label{ssec:feature_sel_lda}
\gls{LDA} is an approach to reduce the dimensionality (number of features) of the data by finding linear combination of features that best separate the classes. For this \gls{LDA} requires the actual data together with the class labels making it only suitable for supervised classification.\\
For reducing the dimensionality, the variance of the data within each class (within class scatter, $S_W$) and the variance between different classes (between class scatter, $S_B$) is calculated. The between class scatter represents the distance between the class means, the within class scatter is the spread of the data points for each class.
Using both scatter matrices following eigenvalue ($\lambda$) eigenvector ($v$) problem can be solved:
$$
S_W^{-1}S_Bv = \lambda v
$$
After sorting the eigenvalues by value the corresponding top highest eigenvectors are used to transform the data. Important to note is that a maximum of $number~classes~-~1$ eigenvalues can be selected due to the rank of the between classes scatter matrix having the sum of the rank of the classes scatter matrices, which themself have a rank between 0 and 1. The number of eigenvalues can not be higher than the rank of the corresponding matrix.\footnote{\url{https://sebastianraschka.com/Articles/2014_python_lda.html}} Using the eigenvectors to transform the original data results in a maximized separation of the classes.\\

A numpy implementation with all the steps and more comments for the \gls{LDA} is done in \textbf{feature\_selection\_own.py} function \textbf{linear\_discriminant\_analysis\_transform\_data}. This implementation is based on eigenvalues and assumes the matrix is a real symmetric matrix, which is not quiet correct.\\

Figure \ref{img:3d_plot_lda_three_components} shows the resulting data and class separation based on a \gls{LDA} with three components. The data points belonging to each class are closely placed together, while the different classes are separated as much as possible. The separation resembles a sphere, with the \gls{LDA} algorithm trying to place the different classes on this sphere. While with three components activities \textit{sitting} and \textit{standing} are still very close together, this separation should become more clear with more components.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/3d_plot_lda_three_components.png}
	\caption{LDA three components}
	\label{img:3d_plot_lda_three_components}
\end{figure}

With sklearn another \gls{LDA} implementation is used with singular value decomposition as solver because the eigenvalue implementation is not able to fit on the used data due to the above mentioned requirement of the matrix being a real symmetric matrix. In the manual implementation this requirement is ignored. For the main model the sklearn \gls{LDA} model is used with $5$ components.\\

The fitted \gls{LDA} classifier can be used as feature selector by transforming the actual data and passing it to another model, but also as model for prediction itself. Here both use cases will be applied, see \ref{sssec:model_lda} for the model.



\subsubsection{Principal component analysis}
\gls{PCA} reduces the dimensionality of the data by transforming the data into a new coordinate system without requiring labelled data. The goal is to transform the data into a coordinate system which reduces the number of dimensions while maximizing the separability of the data. This also is the main difference to \gls{LDA} in section \ref{ssec:feature_sel_lda}. \gls{LDA} tries to maximize the discrimination between classes while \gls{PCA} tries to maximize the variance in the data.\footnote{\url{https://sebastianraschka.com/Articles/2014_pca_step_by_step.html}}.\\

For achieving this the eigenvalues and eigenvectors for the covariance matrix of the data are calculated and the top $n$ eigenvectors are selected, based on the eigenvalue, see \textbf{feature\_selection\_own.py} function \textbf{principal\_component\_analysis\_transform\_data} for the implementation. Figure \ref{img:line_chart_pca_eigenvalues_sorted} shows the resulting eigenvalues sorted by value. The number of eigenvectors selected should be based on the contained variance, meaning those eigenvectors should be used where the eigenvalue has a high value. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/line_chart_pca_eigenvalues_sorted.png}
	\caption{Eigenvalues sorted}
	\label{img:line_chart_pca_eigenvalues_sorted}
\end{figure}

Based on the eigenvalues around 10 eigenvectors are selected for further evaluation. By selecting only three eigenvectors the data can be plotted in a 3D plot, see figure \ref{img:3d_plot_pca_three_components}. Here the separation is not focused on the different classes, which the algorithm obviously does not know, but about a maximum variance of the data. The different classes are nontheless located in proximate locations, showing that a good classification with the available data should be possible.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/3d_plot_pca_three_components.png}
	\caption{PCA three components}
	\label{img:3d_plot_pca_three_components}
\end{figure}

\subsubsection{Recursive feature elimination}
Instead of the more mathematical approaches for feature reduction above, \gls{RFE} eliminates features by actually fitting a machine learning model and removing the least importance features in a loop until the specified number of features is left. Based on the machine learning estimator used, either the $coef\_$ or $feature\_importances\_$ attribute is used for deciding the importance of each feature. \\

In this project a \gls{SVC} with a linear kernel is used as an estimator resulting in the $coef\_$ attribute being used for feature ranking. The $coef\_$ attribute represents the weight of each feature which is calculated during fitting of the model. Those weights itself are based on a 1 vs 1 comparison between all features and the support vectors of the \gls{SVC}, see also \ref{sssec:model_svc}.\\

Running \gls{RFE} with a maximum of three features allows the data to be plotted and already shows a good separation between the classes using those three most relevant features, see figure \ref{img:3d_plot_three_features_rfe}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/3d_plot_three_features_rfe.png}
	\caption{RFE three features}
	\label{img:3d_plot_three_features_rfe}
\end{figure}

\gls{RFE} can be combined with cross validation, \gls{RFECV}, where the optimal number of features is automatically determined by recalculating the model in a loop with less and less features till the accuracy sinks below a threshold score. Applied on our data set this results in 20 features being selected. % $acc\_body\_X\_median$, $acc\_body\_X\_kurtosis$, $acc\_body\_X\_max\_to\_min$, $acc\_body\_X\_max\_to\_min\_diff$, $acc\_body\_Y\_std$, $acc\_body\_Y\_max\_to\_min\_diff$, $acc\_body\_Z\_std$, $gyro\_X\_mean$, $gyro\_X\_std$, $gyro\_X\_iqr$, $gyro\_X\_max\_to\_min\_diff$, $gyro\_Z\_std$, $acc\_gravity\_X\_mean$, $acc\_gravity\_X\_energy$, $acc\_gravity\_Y\_min$, $acc\_gravity\_Y\_max$, $acc\_gravity\_Y\_std$, $acc\_body\_corr\_xy$, $gyro\_x\_y\_angle$, $gyro\_x\_z\_angle$



