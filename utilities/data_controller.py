import pandas as pd
import glob


def get_all_acc_and_gyro_data_from_txt(filepath):
    all_acc_data = [pd.read_csv(file,
                                delim_whitespace=True,
                                header=None, names=['acc_X', 'acc_Y', 'acc_Z']) for file in
                    glob.glob(filepath + 'Raw-Data/acc_*.txt')]

    all_gyro_data = [pd.read_csv(file,
                                 delim_whitespace=True,
                                 header=None, names=['gyro_X', 'gyro_Y', 'gyro_Z']) for file in
                     glob.glob(filepath + 'Raw-Data/gyro_*.txt')]

    return all_acc_data, all_gyro_data


def get_label_and_activity_indexes_from_txt(filepath):
    label_translation = pd.read_csv(filepath + 'activity_labels.txt',
                                    delim_whitespace=True,
                                    header=None, names=['activity_id', 'activity'])
    activity_indexes = pd.read_csv(filepath + 'Raw-Data/labels.txt',
                                   delim_whitespace=True,
                                   header=None, names=['counter', 'user_id', 'activity_id', 'start', 'end'])

    return label_translation, activity_indexes


# Current data are just the measurements of multiple experiments with different activities
# Split measurements based on activity index and create a data object for each activity
def transform_raw_data_into_data_object(label_translation, activity_indexes, all_acc_data, all_acc_gravity_data, all_acc_body_data,
                                        all_gyro_data, all_acc_noise_data, include_gravity_data=True,
                                        include_noise_data=False, include_body_data=True, include_raw_acc_data=False):
    # TODO: this function should also work without filtered data
    data = []
    for index, row in label_translation.iterrows():
        data.append({'activity_id': row['activity_id'], 'name': row['activity'], 'data': []})

    for index, row in activity_indexes.iterrows():
        current_file = row['counter'] - 1
        activity = row['activity_id']
        start = row['start']
        end = row['end']
        acc_gravity_data = pd.DataFrame(all_acc_gravity_data[current_file][start:end].values,
                                        columns=['acc_gravity_X', 'acc_gravity_Y', 'acc_gravity_Z'])
        acc_body_data = pd.DataFrame(all_acc_body_data[current_file][start:end].values,
                                     columns=['acc_body_X', 'acc_body_Y', 'acc_body_Z'])
        gyro_data = pd.DataFrame(all_gyro_data[current_file][start:end].values, columns=['gyro_X', 'gyro_Y', 'gyro_Z'])

        all_data = gyro_data.copy()

        if include_body_data:
            all_data = all_data.join(acc_body_data)

        if include_raw_acc_data:
            raw_data = pd.DataFrame(all_acc_data[current_file][start:end].values, columns=['acc_X', 'acc_Y', 'acc_Z'])
            all_data = all_data.join(raw_data)
        if include_gravity_data:
            all_data = all_data.join(acc_gravity_data)
        if include_noise_data:
            acc_noise_data = pd.DataFrame(all_acc_noise_data[current_file][start:end].values,
                                          columns=['acc_noise_X', 'acc_noise_Y', 'acc_noise_Z'])
            all_data = all_data.join(acc_noise_data)

        activity_index_data = next((index for (index, d) in enumerate(data) if d["activity_id"] == activity), None)
        data[activity_index_data]['data'].append(all_data)

    return data



