import numpy as np


def variance_threshold_return_columns_above_threshold(data, column_names, threshold=0.9):
    feature_variances = np.var(data, axis=0)
    count_above_threshold = sum(1 for value in feature_variances if value > threshold)
    print("variance_threshold_return_columns_above_threshold count:", count_above_threshold)
    return [column for column, value in zip(column_names, feature_variances) if value > threshold]



def linear_discriminant_analysis_transform_data(X_data, labels, n_components=5):
    # based upon: https://ml-explained.com/blog/linear-discriminant-analysis-explained
    # TODO: https://www.python-engineer.com/courses/mlfromscratch/14-lda/
    # 1. calculate mean values
    mean_values = np.mean(X_data, axis=0)

    # 2. compute scatter matrix
    num_classes = len(np.unique(labels))
    num_features = X_data.shape[1]
    # initialize zero matrices for scatter matrices
    within_class_scatter_matrix = np.zeros((num_features, num_features))
    between_class_scatter_matrix = np.zeros((num_features, num_features))

    for current_class in range(num_classes):
        # get data points of current class
        X_current_class = X_data[labels == current_class]
        # calculate mean value of current class
        mean_current_class = np.mean(X_current_class, axis=0)
        # we can use below equation only if the features are equally distributed for each class
        # because this is not the case, we have to divide each sum by the number of class samples.
        # This basically is the covariance matrix
        # within_class_scatter_matrix += (X_current_class - mean_current_class).T.dot(X_current_class - mean_current_class)
        within_class_scatter_matrix += np.cov(X_current_class.T)

        # calculate between scatter matrix
        current_class_mean_diff = (mean_current_class - mean_values).reshape(num_features, 1)
        between_class_scatter_matrix += len(X_current_class) * (current_class_mean_diff.dot(
            current_class_mean_diff.T))

    # 3. compute eigenvalues and eigenvectors
    inv_within_class_scatter_matrix = np.linalg.inv(within_class_scatter_matrix)
    # compute eigenvalues and eigenvectors of the dot product of the two scatter matrices
    # Note: use eigh instead of eig to avoid complex eigenvalues.
    # This theoretically requires the matrix to be a real symmetric matrix, but can be applied even if it is not the case.
    eigenvalues, eigenvectors = np.linalg.eigh(inv_within_class_scatter_matrix.dot(between_class_scatter_matrix))

    # we want to use the real part only. ("Imaginary data can happen if the covariance matrix is not symmetric") --> solved using eigh
    # eigenvalues = np.real(eigenvalues)
    # eigenvectors = np.real(eigenvectors)

    # 4. sort eigenvalues and eigenvectors
    sorted_ev = np.argsort(abs(eigenvalues))[::-1]
    eigenvalues_sorted = eigenvalues[sorted_ev]
    eigenvectors_sorted = eigenvectors[:, sorted_ev]

    # 5. select top 5 eigenvectors and transform data
    top_eigenvectors = eigenvectors_sorted[:, 0:n_components]
    return X_data.dot(top_eigenvectors), eigenvalues_sorted


def principal_component_analysis_transform_data(X_data, n_components=10):
    # 1. calculate mean of data
    X_mean = np.mean(X_data, axis=0)
    # 2. calculate covariance matrix
    X_cov = np.cov(X_data.T)
    # 3. calculate eigenvalues and eigenvectors
    eigenvalues, eigenvectors = np.linalg.eigh(X_cov)
    # 4. sort eigenvalues and eigenvectors
    sorted_ev = np.argsort(abs(eigenvalues))[::-1]
    eigenvalues_sorted = eigenvalues[sorted_ev]
    eigenvectors_sorted = eigenvectors[:, sorted_ev]
    # plt.title('Eigenvalues sorted')
    # plt.plot(eigenvalues_sorted)
    # plt.xlabel('Eigenvalue')
    # plt.show()
    # 5. select n components
    eigenvectors_selected = eigenvectors_sorted[:, 0:n_components]
    # 6. transform data
    transformed_data = np.dot(X_data - X_mean, eigenvectors_selected)
    return transformed_data
