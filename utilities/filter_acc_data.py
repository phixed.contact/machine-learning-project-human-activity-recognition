from scipy.signal import butter, filtfilt
import pandas as pd


def get_gravitational_and_body_components(acc_data, fs, low_cutoff_hz, high_cutoff_hz, order=3):
    nyquist = 0.5 * fs
    low_cutoff = low_cutoff_hz / nyquist
    high_cutoff = high_cutoff_hz / nyquist
    b, a = butter(order, low_cutoff, btype='low', output='ba')
    gravity_data = pd.DataFrame()
    gravity_data['acc_X'] = filtfilt(b, a, acc_data['acc_X'])
    gravity_data['acc_Y'] = filtfilt(b, a, acc_data['acc_Y'])
    gravity_data['acc_Z'] = filtfilt(b, a, acc_data['acc_Z'])

    b, a = butter(order, high_cutoff, btype='high', output='ba')
    noise_data = pd.DataFrame()
    noise_data['acc_X'] = filtfilt(b, a, acc_data['acc_X'])
    noise_data['acc_Y'] = filtfilt(b, a, acc_data['acc_Y'])
    noise_data['acc_Z'] = filtfilt(b, a, acc_data['acc_Z'])

    body_data = pd.DataFrame()
    body_data['acc_X'] = acc_data['acc_X'] - noise_data['acc_X'] - gravity_data['acc_X']
    body_data['acc_Y'] = acc_data['acc_Y'] - noise_data['acc_Y'] - gravity_data['acc_Y']
    body_data['acc_Z'] = acc_data['acc_Z'] - noise_data['acc_Z'] - gravity_data['acc_Z']

    return gravity_data, body_data, noise_data
