import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def generate_static_features(data_xyz):
    static_features = {}
    for column in data_xyz.columns:
        static_features[column + '_min'] = data_xyz[column].min()
        static_features[column + '_max'] = data_xyz[column].max()
        static_features[column + '_mean'] = data_xyz[column].mean()
        static_features[column + '_std'] = data_xyz[column].std()
        static_features[column + '_median'] = data_xyz[column].median()
        static_features[column + '_var'] = data_xyz[column].var()
        static_features[column + '_skew'] = data_xyz[column].skew()
        static_features[column + '_kurtosis'] = data_xyz[column].kurtosis()
        static_features[column + '_iqr'] = np.subtract(*np.percentile(data_xyz[column], [75, 25]))
        static_features[column + '_energy'] = np.sum(np.square(data_xyz[column]))
        static_features[column + '_max_to_min'] = data_xyz[column].max() / data_xyz[column].min()
        static_features[column + '_max_to_min_diff'] = data_xyz[column].max() - data_xyz[column].min()

    return static_features


def generate_xyz_features(name, x, y, z):
    xyz_data = pd.DataFrame({'x': x, 'y': y, 'z': z})
    xyz_features = {}

    temp_corr = xyz_data.corr(min_periods=3)
    xyz_features[name + '_corr_xy'] = temp_corr['x']['y']
    xyz_features[name + '_corr_xz'] = temp_corr['x']['z']
    xyz_features[name + '_corr_yz'] = temp_corr['y']['z']

    xyz_features[name + '_magnitude_mean'] = np.mean(np.sqrt(np.square(xyz_data).sum(axis=1)))

    xyz_features[name + '_x_y_angle'] = np.arctan2(xyz_data['x'], xyz_data['y']).mean()
    xyz_features[name + '_x_z_angle'] = np.arctan2(xyz_data['x'], xyz_data['z']).mean()
    xyz_features[name + '_y_z_angle'] = np.arctan2(xyz_data['y'], xyz_data['z']).mean()

    return xyz_features


def get_magnitude_and_angle_as_series(name, x, y, z):
    xyz_data = pd.DataFrame({'x': x, 'y': y, 'z': z})
    result = xyz_data[['x']].copy()

    result[name + '_magnitude'] = np.sqrt(np.square(xyz_data).sum(axis=1))
    result[name + '_x_y_angle'] = np.arctan2(xyz_data['x'], xyz_data['y'])
    result.drop('x', axis=1, inplace=True)

    return result


def generate_features_for_static_timeseries_window(data):
    new_row = {}
    temp = generate_static_features(data)
    new_row = {**new_row, **temp}
    if "acc_body_X" in data:
        temp = generate_xyz_features('acc_body', data['acc_body_X'].values, data['acc_body_Y'].values,
                                     data['acc_body_Z'].values)
        new_row = {**new_row, **temp}
    if "acc_gravity_X" in data:
        temp = generate_xyz_features('acc_gravity', data['acc_gravity_X'].values,
                                     data['acc_gravity_Y'].values,
                                     data['acc_gravity_Z'].values)
        new_row = {**new_row, **temp}
    if "gyro_X" in data:
        temp = generate_xyz_features('gyro', data['gyro_X'].values, data['gyro_Y'].values,
                                     data['gyro_Z'].values)
        new_row = {**new_row, **temp}
    if "acc_X" in data:
        temp = generate_xyz_features('acc', data['acc_X'].values, data['acc_Y'].values,
                                     data['acc_Z'].values)
        new_row = {**new_row, **temp}

    return new_row


def generate_features_for_each_activity(data, activities_to_use):
    static_data = None
    for activity in data:
        if activity['name'] not in activities_to_use:
            continue
        new_row = {}
        new_row['activity_name'] = activity['name']
        for entry in activity['data']:
            # print(entry)
            temp = generate_static_features(pd.DataFrame(entry))
            new_row = {**new_row, **temp}
            if "acc_body_X" in entry:
                temp = generate_xyz_features('acc_body', entry['acc_body_X'].values, entry['acc_body_Y'].values,
                                             entry['acc_body_Z'].values)
                new_row = {**new_row, **temp}
            if "acc_gravity_X" in entry:
                temp = generate_xyz_features('acc_gravity', entry['acc_gravity_X'].values,
                                             entry['acc_gravity_Y'].values,
                                             entry['acc_gravity_Z'].values)
                new_row = {**new_row, **temp}
            if "gyro_X" in entry:
                temp = generate_xyz_features('gyro', entry['gyro_X'].values, entry['gyro_Y'].values,
                                             entry['gyro_Z'].values)
                new_row = {**new_row, **temp}
            if "acc_X" in entry:
                temp = generate_xyz_features('acc', entry['acc_X'].values, entry['acc_Y'].values,
                                             entry['acc_Z'].values)
                new_row = {**new_row, **temp}

            if static_data is None:
                static_data = pd.DataFrame(columns=new_row.keys())
            static_data.loc[len(static_data.index)] = new_row

    return static_data


def transform_text_labels_into_numbers(labels):
    y = []
    for label in labels:
        if label == 'WALKING':
            y.append(0)
        elif label == 'WALKING_UPSTAIRS':
            y.append(1)
        elif label == 'WALKING_DOWNSTAIRS':
            y.append(2)
        elif label == 'SITTING':
            y.append(3)
        elif label == 'STANDING':
            y.append(4)
        elif label == 'LAYING':
            y.append(5)
        else:
            print('unknown label', label)
    return y


def create_X_and_y(static_data):
    X_temp = static_data.drop('activity_name', axis=1)
    X_column_names = X_temp.columns
    X = np.array(X_temp)
    X = np.nan_to_num(X)
    labels = np.array(static_data['activity_name'])
    y = transform_text_labels_into_numbers(labels)
    y = np.array(y)

    return X, y, X_column_names


def generate_train_test_split_and_scale_data(X, y, test_size=0.2, random_state=1905, scale_data=True):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    # Fit StandardScaler on X_train only and use it to transform X_train and X_test
    # "data transformations should be learnt from a training set and applied to held-out data for prediction"
    # https://scikit-learn.org/stable/modules/cross_validation.html
    if scale_data:
        scaler = StandardScaler().fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)
    return X_train, X_test, y_train, y_test
