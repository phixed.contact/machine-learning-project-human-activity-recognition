# Human Activity Recognition
Following Repository analyses the Human Activity Recognition Dataset using Machine Learning Algorithms.
The dataset was analysed for the course machine learning at the university of verona, fall semester 2023/2024
* Dataset: https://github.com/anas337/Human-Activity-Recognition-Using-Smartphones.github.io
  * `Jorge-L. Reyes-Ortiz, Luca Oneto, Albert Samà, Xavier Parra, Davide Anguita. Transition-Aware Human Activity Recognition Using Smartphones. Neurocomputing. Springer 2015.`
* the attached requirements.txt can be used with conda to create an environment and install all required packages


## Structure
### Main folder with main analysis
* basic_analysis: some basic analysis and plots of the data
* static_analysis: analysis of the data without using time series
* timeseries_analysis_svm: analysis of the data using time series and SVM
* timeseries_analysis_lda: online prediction by using time series of different sizes, applying statical feature generation and using LDA for prediction


### utilities
* data_controller: for loading data and transforming it into a usable matrix with labels
* feature_generation: for generating features from the data
* filter_acc_data: for filtering accelerometer data using butterworth filter
* feature_selection_own: feature selection algorithm implemented using numpy

### report
* Latex files for report for this project

### presentation
* Power point presentation of this project



## Disclaimer
Free usage of any parts of this repository, no citing required.

by Phil Borkenhagen